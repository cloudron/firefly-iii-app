#!/bin/bash

set -eu

mkdir -p /run/firefly-iii/framework-cache /run/firefly-iii/logs /run/firefly-iii/bootstrap-cache

if [[ ! -d /app/data/storage ]]; then
    echo "==> Creating storage directory"
    mkdir /app/data/storage
    cp -r /app/code/storage.original/* /app/data/storage

fi

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

[[ ! -f /app/data/env ]] && cp /app/pkg/env.production /app/data/env

if [[ -n "${CLOUDRON_PROXY_AUTH:-}" ]]; then
    # https://docs.firefly-iii.org/firefly-iii/advanced-installation/authentication/
    sed -e 's/AUTHENTICATION_GUARD=.*/AUTHENTICATION_GUARD=remote_user_guard/g' \
        -e 's,CUSTOM_LOGOUT_URL=.*,CUSTOM_LOGOUT_URL=/logout,g' \
        -i /app/data/env
else
    sed -e 's/AUTHENTICATION_GUARD=.*/AUTHENTICATION_GUARD=web/g' -i /app/data/env
fi

table_count=$(PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -AXqtc "SELECT COUNT(*) FROM pg_tables WHERE schemaname = 'public';")
rm -f /app/data/.initialized # legacy

if [[ "${table_count}" == "0" ]]; then
    # https://docs.firefly-iii.org/how-to/firefly-iii/installation/self-managed/
    echo "==> Initializing database on first run"
    php artisan migrate:refresh --seed
    php artisan firefly-iii:upgrade-database
    php artisan firefly-iii:correct-database
    php artisan firefly-iii:report-integrity
    php artisan key:generate # Create a random APP_KEY and set it in our env file
    php artisan firefly-iii:laravel-passport-keys

    # disable update check -REPLACE https://github.com/firefly-iii/firefly-iii/issues/1050#issuecomment-352727173
    PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} \
        -c "INSERT INTO configuration (name, data, created_at) VALUES ('permission_update_check', '0', '2020-01-21 20:00:00')"
else
    # https://docs.firefly-iii.org/advanced-installation/upgrade
    # view:clear is not enough. otherwise we get some twig errors (https://github.com/firefly-iii/firefly-iii/issues/1936)
    rm -rf /app/data/storage/framework/views && mkdir -p /app/data/storage/framework/views/twig /app/data/storage/framework/views/v1

    echo "==> Upgrading database"
    php artisan migrate --seed
    php artisan cache:clear
    php artisan firefly-iii:upgrade-database
    php artisan firefly-iii:laravel-passport-keys
    php artisan cache:clear
    php artisan view:clear
fi

# sessions, logs and cache 
rm -rf /app/data/storage/framework/cache && ln -s /run/firefly-iii/framework-cache /app/data/storage/framework/cache
rm -rf /app/data/storage/logs && ln -s /run/firefly-iii/logs /app/data/storage/logs

echo "==> Changing permissions"
chown -R www-data:www-data /app/data/ /run/firefly-iii/

echo "==> Staring Firefly III"

APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
