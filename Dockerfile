FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

RUN apt-get remove -y php-* php8.1-* libapache2-mod-php8.1 && \
    apt-get autoremove -y && \
    add-apt-repository --yes ppa:ondrej/php && \
    apt update && \
    apt install -y php8.4 php8.4-{apcu,bcmath,bz2,cgi,cli,common,curl,dba,dev,enchant,fpm,gd,gmp,gnupg,imagick,imap,interbase,intl,ldap,mailparse,mbstring,mysql,odbc,opcache,pgsql,phpdbg,pspell,readline,redis,snmp,soap,sqlite3,sybase,tidy,uuid,xml,xmlrpc,xsl,zip,zmq} libapache2-mod-php8.4 && \
    apt install -y php-{date,pear,twig,validate} && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# this binaries are not updated with PHP_VERSION since it's a lot of work
RUN update-alternatives --set php /usr/bin/php8.4 && \
    update-alternatives --set phar /usr/bin/phar8.4 && \
    update-alternatives --set phar.phar /usr/bin/phar.phar8.4 && \
    update-alternatives --set phpize /usr/bin/phpize8.4 && \
    update-alternatives --set php-config /usr/bin/php-config8.4

# renovate: datasource=github-releases depName=firefly-iii/firefly-iii versioning=semver extractVersion=^v(?<version>.+)$
ARG FIREFLY_VERSION=6.2.9

# Install Firefly-III
RUN cd /tmp && \
    wget https://github.com/firefly-iii/firefly-iii/releases/download/v${FIREFLY_VERSION}/FireflyIII-v${FIREFLY_VERSION}.zip && \
    unzip FireflyIII-v${FIREFLY_VERSION}.zip -d /app/code && \
    rm FireflyIII-v${FIREFLY_VERSION}.zip

RUN chmod -R g+w /app/code/bootstrap/cache
# Make a symlink for our storage directory and .env file to /app/data/ so we can write to it
RUN mv /app/code/storage /app/code/storage.original && \
    ln -s /app/data/storage /app/code/storage && \
    mv /app/code/bootstrap/cache /app/code/bootstrap/cache.template && ln -s /run/firefly-iii/bootstrap-cache /app/code/bootstrap/cache

RUN rm -f /app/code/.env && ln -sf /app/data/env /app/code/.env

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf
COPY apache/firefly-iii.conf /etc/apache2/sites-enabled/firefly-iii.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

RUN a2disconf other-vhosts-access-log
# disable perl for php locale to work
RUN a2enmod rewrite php8.4 && a2dismod perl

RUN crudini --set /etc/php/8.4/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/8.4/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/8.4/apache2/php.ini PHP memory_limit 64M && \
    crudini --set /etc/php/8.4/apache2/php.ini Session session.save_path /run/php/session && \
    crudini --set /etc/php/8.4/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.4/apache2/php.ini Session session.gc_divisor 100

RUN ln -s /app/data/php.ini /etc/php/8.4/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.4/cli/conf.d/99-cloudron.ini

COPY start.sh env.production /app/pkg/

CMD [ "/app/pkg/start.sh" ]
