#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 5000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TIMEOUT);
    }

    async function login(username, password, session = true) {
        await browser.get(`https://${app.fqdn}/login`);
        await waitForElement(By.id('loginProceedButton'));
        await browser.findElement(By.id('loginProceedButton')).click();

        if (!session) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await browser.wait(until.elementLocated(By.xpath('//span[text()="Dashboard"]')), TIMEOUT);
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/logout`);
        await waitForElement(By.id('loginProceedButton'));
    }

    async function createUser() {
        const email = 'admin@example.com';
        const password = 'thispasswordneedstobe16characterslong';

        await browser.get(`https://${app.fqdn}/register`);
        await browser.wait(until.elementLocated(By.xpath('//input[@name="email"]')), TIMEOUT);
        await browser.findElement(By.xpath('//input[@name="email"]')).sendKeys(email);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.findElement(By.xpath('//input[@name="password_confirmation"]')).sendKeys(password);
        await browser.findElement(By.xpath('//button[text()="Register"]')).click();
        await browser.wait(until.elementLocated(By.xpath('//h3[text()="Getting started"]')), TIMEOUT);
    }

    async function initializeAccount() {
        await browser.get('https://' + app.fqdn);
        await browser.findElement(By.id('ffInput_bank_name')).sendKeys('Cloudron Bank');
        await browser.findElement(By.id('bank_balance')).sendKeys('0');
        await browser.findElement(By.xpath('//input[@type="submit" and contains(@class, "btn-success")]')).click();
        await browser.sleep(3000);
        if (app.manifest.version === '3.5.0') {
            await browser.wait(until.elementLocated(By.xpath('//span[text()="Dashboard"]')), TIMEOUT);
        } else {
            const alertText = await browser.findElement(By.xpath('//div[contains(@class, "alert")]')).getText();
            if (!alertText.includes('Your new accounts have been stored')) throw new Error('Wrong success message');
            await browser.findElement(By.xpath('//a[text()="Skip"]')).click(); // skip intro
        }
    }

    async function checkAccount() {
        await browser.get('https://' + app.fqdn + '/accounts/asset');
        await browser.wait(until.elementLocated(By.xpath('//a[text()="Cloudron Bank"]')), TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.startsWith(LOCATION); })[0];
        expect(app).to.be.an('object');
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login', login.bind(null, username, password, false));
    it('can initialize when user logs in for the first time', initializeAccount);
    it('can logout', logout);

    it('can restart app', async function () { execSync(`cloudron restart --app ${app.id}`); });

    it('can login', login.bind(null, username, password));
    it('has the bank account', checkAccount);
    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('has the bank account', checkAccount);

    it('restore app', async function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('has the bank account', checkAccount);

    it('uninstall app', function () { execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS); });

    // no sso test
    it('install app (no sso)', function () { execSync(`cloudron install --no-sso --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can create user', createUser);
    it('can initialize when user logs in for the first time', initializeAccount);
    it('uninstall app', function () { execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS); });

    // test update
    it('can install previous version from appstore', function () {
        execSync('cloudron install --appstore-id org.fireflyiii.cloudronapp --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
    });
    it('can login', login.bind(null, username, password, true));
    it('can initialize when user logs in for the first time', initializeAccount);

    it('can update', function () { execSync(`cloudron update --app ${LOCATION}`, EXEC_ARGS); });

    it('get app info', getAppInfo);
    it('has the bank account', checkAccount);

    it('uninstall app', function () { execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS); });
});
